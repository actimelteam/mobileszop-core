package zpi.adminwebshop.utils;


import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import zpi.adminwebshop.category.Category;
import zpi.adminwebshop.category.CategoryService;
import zpi.adminwebshop.order.Order;
import zpi.adminwebshop.order.OrderPositions;
import zpi.adminwebshop.order.OrderService;
import zpi.adminwebshop.product.Product;
import zpi.adminwebshop.product.ProductService;
import zpi.adminwebshop.shop.Shop;
import zpi.adminwebshop.shop.ShopService;
import zpi.adminwebshop.user.User;
import zpi.adminwebshop.user.UserService;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {

    private UserService userService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private ProductService productService;
    private CategoryService categoryService;
    private OrderService orderService;
    private ShopService shopService;

    public DataLoader(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder, ProductService productService, CategoryService categoryService, OrderService orderService, ShopService shopService) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.productService = productService;
        this.categoryService = categoryService;
        this.orderService = orderService;
        this.shopService = shopService;
    }

    @Override
    public void run(String... args) {
        createUsers();
        createShop();
    }

    private void createShop() {
        Shop shop = new Shop();
        shop.setName("SzopMobile").setEndOrderingTime("16:00")
                .setStartCollectingTime("16:00")
                .setEndCollectingTime("20:00")
                .setLastUpdateDate(new Date());
        shopService.save(shop);
    }

    private void createUsers() {
        User seller = new User();
        seller.setEmail("seller@domain.com").setPassword(bCryptPasswordEncoder.encode("password")).setRole("SELLER");
        userService.save(seller);
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setEmail("user" + i + "@user.com").setPassword(bCryptPasswordEncoder.encode("password")).setRole("USER");
            userService.save(user);
        }
    }

    private void createProducts() {
        List<Category> categories = categoryService.findAll();
        for (int i = 0; i < 100; i++) {
            Collections.shuffle(categories);
            Product product = new Product();
            product.setName("product " + i).setCategory(categories.get(0)).setDescription("Product for testing").setPrice(i + 1)
                    .setStatus(Product.Status.AVAILABLE).setUnit(Product.Unit.PIECE);
            productService.save(product, null);
        }
    }

    private void createOrders() {

        for (int i = 0; i < 100; i++) {
            Order order = new Order();
            order.setPlannedCollectionDate(new Date());
            order.setStatus(Order.OrderStatus.REALISED);
            order.setSubmissionDate(new Date());
            order.setUser(userService.findByEmail("user1@user.com"));
            OrderPositions orderPositions = new OrderPositions();
            orderPositions.setProduct(productService.findById((long) 1).get());
            orderPositions.setAmount(3);
            orderPositions.setPrice(10);
            orderPositions.setOrder(order);
            HashSet<OrderPositions> orderPositionsHashSet = new HashSet<>();
            orderPositionsHashSet.add(orderPositions);
            order.setOrderPositionsSet(orderPositionsHashSet);
            orderService.saveOrder(order);
        }
    }
}
