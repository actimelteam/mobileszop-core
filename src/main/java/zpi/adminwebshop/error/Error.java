package zpi.adminwebshop.error;

public class Error {
    public static int UNEXPECTED = 0;
    public static int CATEGORY_DELETION = 1;
    public static int NO_SUCH_ORDER = 2;
    public static int NO_SUCH_PRODUCT = 3;
    public static int NO_SUCH_CATEGORY = 4;
    public static int INVALID_BARCODE = 5;
}
