package zpi.adminwebshop.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import zpi.adminwebshop.configuration.properties.AuthenticationProperties;
import zpi.adminwebshop.configuration.properties.StorageProperties;

@Configuration
@EnableConfigurationProperties({StorageProperties.class, AuthenticationProperties.class})
public class PropertiesConfiguration {
}
