package zpi.adminwebshop.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("adminwebshop.authorization")
public class AuthenticationProperties {
    private String googleClientID;

    public String getGoogleClientID() {
        return googleClientID;
    }

    public void setGoogleClientID(String googleClientID) {
        this.googleClientID = googleClientID;
    }
}
