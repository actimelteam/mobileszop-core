package zpi.adminwebshop.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("adminwebshop.storage")
public class StorageProperties {
    private String photosDir;

    public StorageProperties() {
        this.photosDir = System.getProperty("user.dir") + "\\photos";
    }

    public String getPhotosDir() {
        return photosDir;
    }

    public void setPhotosDir(String photosDir) {
        this.photosDir = photosDir;
    }
}
