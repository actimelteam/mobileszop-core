package zpi.adminwebshop.user;

import zpi.adminwebshop.user.Exception.InvalidTokenException;
import zpi.adminwebshop.user.Exception.InvalidUserException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Optional;

public interface AuthenticationService {
    Optional<User> getUserFromGoogleToken(String idToken) throws GeneralSecurityException, IOException, InvalidUserException, InvalidTokenException;
}
