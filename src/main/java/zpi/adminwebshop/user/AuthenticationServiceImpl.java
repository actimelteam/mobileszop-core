package zpi.adminwebshop.user;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zpi.adminwebshop.configuration.properties.AuthenticationProperties;
import zpi.adminwebshop.user.Exception.InvalidTokenException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationProperties authenticationProperties;
    private GoogleIdTokenVerifier googleIdTokenVerifier;
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public AuthenticationServiceImpl(AuthenticationProperties authenticationProperties) {
        this.authenticationProperties = authenticationProperties;

        HttpTransport httpTransport = new ApacheHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        googleIdTokenVerifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory)
                .setAudience(Collections.singletonList(authenticationProperties.getGoogleClientID()))
                .build();
    }

    @Override
    public Optional<User> getUserFromGoogleToken(String idToken) throws GeneralSecurityException, IOException, InvalidTokenException {
        GoogleIdToken token;
        try {
            token = googleIdTokenVerifier.verify(idToken);
        } catch (IllegalArgumentException e) {
            logger.warn("Wrong token", e);
            throw new InvalidTokenException();
        }
        Optional<User> maybeUser = Optional.empty();
        if (token != null) {
            User user = new User();
            GoogleIdToken.Payload payload = token.getPayload();
            user.setEmail(payload.getEmail());
            user.setName((String) payload.get("given_name"));
            user.setSurname((String) payload.get("family_name"));
            user.setRole("USER");
            maybeUser = Optional.of(user);
        }
        return maybeUser;
    }
}
