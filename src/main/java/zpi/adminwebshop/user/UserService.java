package zpi.adminwebshop.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zpi.adminwebshop.user.Exception.InvalidTokenException;
import zpi.adminwebshop.user.Exception.InvalidUserException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Optional;


@Service
public class UserService {

    private UserRepository userRepository;
    private AuthenticationService authenticationService;

    @Autowired
    public UserService(UserRepository userRepository,
                       AuthenticationService authenticationService) {
        this.userRepository = userRepository;
        this.authenticationService = authenticationService;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User getUserByGoogleTokenAndSaveIfNotExist(String idToken) throws InvalidUserException, GeneralSecurityException, IOException, InvalidTokenException {
        Optional<User> maybeUserFromGoogle = authenticationService.getUserFromGoogleToken(idToken);
        User userFromGoogle = maybeUserFromGoogle.orElseThrow(InvalidUserException::new);
        User user = findByEmail(userFromGoogle.getEmail());
        if (user == null) {
            user = save(userFromGoogle);
        }
        return user;
    }

    public User getUserByGoogleToken(String idToken) throws InvalidTokenException, GeneralSecurityException, InvalidUserException, IOException {
        Optional<User> maybeUserFromGoogle = authenticationService.getUserFromGoogleToken(idToken);
        User userFromGoogle = maybeUserFromGoogle.orElseThrow(InvalidUserException::new);
        return findByEmail(userFromGoogle.getEmail());
    }
}