package zpi.adminwebshop.product;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zpi.adminwebshop.category.Category;

import java.util.Date;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByNameContaining(String name);
    long countByCategory_Id(long category_id);

    List<Product> findByCategory(Category category);
    List<Product> findByLastUpdateDateAfter(Date updateDate);
}
