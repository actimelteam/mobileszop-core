package zpi.adminwebshop.product.Exception;

public class NoSuchProductException extends Exception {
    private long productId;

    public NoSuchProductException() {
    }

    public NoSuchProductException(long productId) {
        this.productId = productId;
    }

    public long getProductId() {
        return productId;
    }
}
