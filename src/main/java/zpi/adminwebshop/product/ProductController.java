package zpi.adminwebshop.product;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zpi.adminwebshop.category.Category;
import zpi.adminwebshop.category.CategoryService;
import zpi.adminwebshop.category.dto.CategoryDtoMapper;
import zpi.adminwebshop.configuration.properties.StorageProperties;
import zpi.adminwebshop.error.Error;
import zpi.adminwebshop.product.dto.ProductDtoMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/products")
public class ProductController {
    private static final int MESSAGE_PRODUCT_SAVED = 0;
    private static final int MESSAGE_PRODUCT_DELETED = 1;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ProductService productService;
    private CategoryService categoryService;
    private final StorageProperties storageProperties;
    private final CategoryDtoMapper categoryDtoMapper;

    @Autowired
    public ProductController(ProductService productService, CategoryService categoryService, StorageProperties storageProperties, CategoryDtoMapper categoryDtoMapper) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.storageProperties = storageProperties;
        this.categoryDtoMapper = categoryDtoMapper;
    }

    @GetMapping("")
    public String showProducts(Model model, @RequestParam(value = "productOrCategoryName", required = false) String q, Pageable pageable) {
        Page<Product> products;
        if (q == null) {
            products = productService.findAll(pageable);
        } else {
            products = productService.findByNameOrCategoryName(q, pageable);
        }
        model.addAttribute("products", products.map(ProductDtoMapper::productToSimpleDto));
        return "product/products";
    }

    @GetMapping("/add")
    public String showAddProductForm(Model model) {
        Product emptyProduct = new Product();
        emptyProduct.setCategory(new Category());
        model.addAttribute("product", emptyProduct);
        model.addAttribute("categories",
                categoryDtoMapper.deepMapCategoriesToSimpleDto(categoryService.getCategoriesWithoutParentCategory()));
        return "product/product_details";
    }

    @GetMapping("/product/{id}")
    public String showProductDetails(@PathVariable Long id, Model model) {
        Optional<Product> supposedProduct = productService.findById(id);
        if (supposedProduct.isPresent()) {
            Product product = supposedProduct.get();
            model.addAttribute("product", product);
            model.addAttribute("categories",
                    categoryDtoMapper.deepMapCategoriesToSimpleDto(categoryService.getCategoriesWithoutParentCategory()));
            model.addAttribute("photoDirPath", storageProperties.getPhotosDir());
            return "product/product_details";
        } else {
            logger.warn("Product with id " + id + " not found");
            model.addAttribute("errorCode", Error.NO_SUCH_PRODUCT);
            return "error";
        }
    }

    @PostMapping("/save")
    public String addProduct(@ModelAttribute Product product,
                             @RequestParam(value = "productPhoto", required = false) MultipartFile photo,
                             @RequestParam(value = "categoryId") Long categoryId,
                             RedirectAttributes redirectAttributes,
                             Model model) {

        if (!product.getBarcode().isEmpty() && !EANBarcodeService.isEANBarcodeValid(product.getBarcode())) {
            Map<String, Integer> errors = new HashMap<>();
            errors.put("barcode", Error.INVALID_BARCODE);
            Category category = new Category();
            category.setId(categoryId);
            product.setCategory(category);
            model.addAttribute("errors", errors);
            model.addAttribute("categories",
                    categoryDtoMapper.deepMapCategoriesToSimpleDto(categoryService.getCategoriesWithoutParentCategory()));
            Product productFromDatabase = productService.getProductById(product.getId());
            String productPhoto = productFromDatabase != null ? productFromDatabase.getPhoto() : null;
            product.setPhoto(productPhoto);
            model.addAttribute("product", product);
            return "product/product_details";
        }

        Optional<Category> supposedCategory = categoryService.findById(categoryId);
        if (supposedCategory.isPresent()) {
            Category category = supposedCategory.get();
            product.setCategory(category);
            productService.save(product, photo);
            redirectAttributes.addFlashAttribute("messageCode", MESSAGE_PRODUCT_SAVED);
            return "redirect:/products";
        } else {
            logger.warn("Category with id " + categoryId + " assigned to product during product saving not found");
            redirectAttributes.addFlashAttribute("errorCode", Error.NO_SUCH_CATEGORY);
            return "redirect:/error";
        }
    }

    @PostMapping("/delete")
    public String deleteProduct(@RequestParam Long id, final RedirectAttributes redirectAttributes) {
        if (!productService.delete(id)) {
            logger.warn("Product with id " + id + " not found during request for deletion");
            redirectAttributes.addFlashAttribute("errorCode", Error.NO_SUCH_PRODUCT);
            return "redirect:/error";
        }
        redirectAttributes.addFlashAttribute("messageCode", MESSAGE_PRODUCT_DELETED);
        return "redirect:/products";
    }

    @GetMapping("productCount/{categoryId}")
    public @ResponseBody Long getProductCountFromCategory(@PathVariable Integer categoryId) {
        return productService.productCountInCategory(categoryId);
    }
}
