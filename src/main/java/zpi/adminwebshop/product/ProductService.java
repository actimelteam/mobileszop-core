package zpi.adminwebshop.product;


import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import zpi.adminwebshop.category.Category;
import zpi.adminwebshop.category.CategoryService;
import zpi.adminwebshop.configuration.properties.StorageProperties;
import zpi.adminwebshop.order.OrderPositionRepository;
import zpi.adminwebshop.order.OrderPositions;
import zpi.adminwebshop.product.Exception.NoSuchProductException;
import zpi.adminwebshop.user.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private StorageProperties storageProperties;
    private OrderPositionRepository orderPositionRepository;
    private CategoryService categoryService;

    public ProductService(ProductRepository productRepository, StorageProperties storageProperties,
                          OrderPositionRepository orderPositionRepository, CategoryService categoryService) {
        this.productRepository = productRepository;
        this.storageProperties = storageProperties;
        this.orderPositionRepository = orderPositionRepository;
        this.categoryService = categoryService;
    }

    public Product getProductById(long id) {
        return productRepository.getOne(id);
    }

    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Page<Product> findByNameOrCategoryName(String name, Pageable pageable) {
        Set<Product> products = new HashSet<>(productRepository.findByNameContaining(name));
        Set<Category> categories = new HashSet<>(categoryService.getCategoriesByNameContaining(name));
        for (Category category : categories) {
            products.addAll(getAllProductsFromCategory(category));
        }
        return new PageImpl<Product>(new ArrayList(products));
    }

    public Product save(Product product, MultipartFile photo) {
        Optional<Product> oldProduct = productRepository.findById(product.getId());
        product.setPhoto(oldProduct.map(Product::getPhoto).orElse(null));
        if (photo != null && !photo.isEmpty()) {
            if (oldProduct.isPresent()) {
                removePhotoIfExist(product.getId());
            }
            Date date = new Date();
            Long time = date.getTime();
            String extension = "";
            int dotIndex = photo.getOriginalFilename().lastIndexOf('.');
            if (dotIndex > 0) {
                extension = photo.getOriginalFilename().substring(dotIndex);
            }
            String photoName = time.toString() + extension;
            product.setPhoto(photoName);
            try {
                photo.transferTo(new File(storageProperties.getPhotosDir() + File.separator + photoName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        product.setLastUpdateDate(new Date());
        return productRepository.save(product);
    }

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    public boolean delete(Long id) {
        try {
            if (orderPositionRepository.countByProduct_Id(id) == 0) {
                if (!removePhotoIfExist(id)) {
                    return false;
                }
                productRepository.deleteById(id);
                return true;
            }
        } catch (EmptyResultDataAccessException exception) {
            return false;
        }
        return false;
    }

    public byte[] getImage(Long productId) throws NoSuchProductException {
        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new NoSuchProductException(productId));
        String photoName = product.getPhoto();
        if (photoName == null) {
            return null;
        }
        String stringPath = storageProperties.getPhotosDir() + File.separator + photoName;
        Path path = Paths.get(stringPath);
        byte[] data = new byte[0];
        try {
            data = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public long productCountInCategory(long categoryID) {
        return productRepository.countByCategory_Id(categoryID);
    }

    private boolean removePhotoIfExist(Long productId) {
        String photoName;
        Optional<Product> maybeProduct = productRepository.findById(productId);

        if (!maybeProduct.isPresent()) {
            return false;
        } else {
            photoName = maybeProduct.get().getPhoto();
        }
        if (photoName != null) {
            String path = storageProperties.getPhotosDir() + File.separator + photoName;
            File file = new File(path);
            return file.delete();
        }
        return true;
    }

    public List<Product> getProductsAfterGivenDate(LocalDateTime date) {
        Instant instant = date.atZone(ZoneId.systemDefault()).toInstant();
        Date updateDate = Date.from(instant);
        return productRepository.findByLastUpdateDateAfter(updateDate);
    }

    private List<Product> getAllProductsFromCategory(Category category) {
        List<Product> products = productRepository.findByCategory(category);
        if (!category.getSubcategories().isEmpty()) {
            for (Category subcategory : category.getSubcategories()) {
                products.addAll(getAllProductsFromCategory(subcategory));
            }
        }
        return products;
    }

    public List<Product> getMostPopularProductsForAll(int top) {
        return getMostPopularProducts(top, orderPositionRepository.findAll());
    }

    public List<Product> getMostPopularProductsForGivenUser(int top, User user) {
        List<OrderPositions> orderPositionsList = orderPositionRepository.findAll().stream()
                .filter(orderPositions -> orderPositions.getOrder().getUser().equals(user)).collect(Collectors.toList());
        return getMostPopularProducts(top, orderPositionsList);
    }

    private List<Product> getMostPopularProducts(int top, List<OrderPositions> orderPositionsList) {
        return orderPositionsList
                .stream().collect(Collectors.groupingBy(OrderPositions::getProduct, Collectors.counting()))
                .entrySet().stream().sorted(Map.Entry.<Product, Long>comparingByValue().reversed())
                .limit(top).map(Map.Entry::getKey).collect(Collectors.toList());
    }


}
