package zpi.adminwebshop.product.dto;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zpi.adminwebshop.category.Category;
import zpi.adminwebshop.category.dto.CategoryDtoMapper;
import zpi.adminwebshop.product.Product;

@Component
public class ProductDtoMapper {

    @Value("${adminwebshop.domain}")
    private String domain;

    public ProductDto productToDto(Product product) {
        String photoUrl;
        if (product.getPhoto() == null) {
            photoUrl = null;
        } else {
            photoUrl = domain + "/api/products/product/" + product.getId() + "/photo";
        }
        return new ProductDto(product, photoUrl);
    }

    public static ProductSimpleDto productToSimpleDto(Product product) {
        ProductSimpleDto simpleProduct = new ProductSimpleDto();
        simpleProduct.setId(product.getId());
        simpleProduct.setName(product.getName());
        simpleProduct.setPrice(product.getPrice());
        simpleProduct.setStatus(product.getStatus());
        simpleProduct.setUnit(product.getUnit());
        simpleProduct.setCategoryFullName(getCategoryFullName(product.getCategory()).substring(CategoryDtoMapper.PATH_SEPARATOR.length() - 1));
        return simpleProduct;
    }

    private static String getCategoryFullName(Category category) {
        if (category == null)
            return "";
        return getCategoryFullName(category.getParentCategory())
                + CategoryDtoMapper.PATH_SEPARATOR + category.getName();
    }
}
