package zpi.adminwebshop.product.dto;

import zpi.adminwebshop.product.Product;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class ProductDto {

    private long id;

    private String name;

    private double price;

    private String description;

    private Product.Unit unit;

    private Product.Status status;

    private long categoryId;

    private String photoUrl;

    private LocalDateTime lastUpdateDate;

    ProductDto(Product product, String photoUrl) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.description = product.getDescription();
        this.unit = product.getUnit();
        this.status = product.getStatus();
        this.categoryId = product.getCategory().getId();
        this.photoUrl = photoUrl;
        this.lastUpdateDate = product.getLastUpdateDate().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public Product.Unit getUnit() {
        return unit;
    }

    public Product.Status getStatus() {
        return status;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public ProductDto setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
        return this;
    }
}
