package zpi.adminwebshop.product;

import org.apache.commons.codec.binary.Base64;
import org.krysalis.barcode4j.impl.upcean.*;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.stereotype.Service;
import zpi.adminwebshop.product.Exception.InvalidEANBarcodeException;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class EANBarcodeService {
    private static final int BARCODE_IMAGE_RESOLUTION = 300;
    private static final int BARCODE_IMAGE_HEIGHT = 12;

    public static boolean isEANBarcodeValid(String barcode) {
        return validateEANBarcode(barcode) != EANBarcodeType.INVALID;
    }

    public static EANBarcodeType validateEANBarcode(String barcode) {
        if (barcode.matches("[0-9]{8}") && isChecksumValid(barcode)) {
            return EANBarcodeType.EAN8;
        }
        if (barcode.matches("[0-9]{13}") && isChecksumValid(barcode)) {
            return EANBarcodeType.EAN13;
        }
        return EANBarcodeType.INVALID;
    }

    public String getBarcodeImageBase64(String barcode) throws InvalidEANBarcodeException, IOException {
        UPCEANBean eanBean;

        switch (validateEANBarcode(barcode)) {
            case EAN8:
                eanBean = new EAN8Bean();
                break;
            case EAN13:
                eanBean = new EAN13Bean();
                break;
            default:
                throw new InvalidEANBarcodeException();
        }

        eanBean.doQuietZone(true);
        eanBean.setHeight(BARCODE_IMAGE_HEIGHT);
        ByteArrayOutputStream barcodeImageOutputStream = new ByteArrayOutputStream();
        BitmapCanvasProvider provider = new BitmapCanvasProvider(
                barcodeImageOutputStream,
                "image/x-png",
                BARCODE_IMAGE_RESOLUTION,
                BufferedImage.TYPE_BYTE_BINARY,
                false,
                0);
        eanBean.generateBarcode(provider, barcode);
        provider.finish();
        barcodeImageOutputStream.close();
        return Base64.encodeBase64String(barcodeImageOutputStream.toByteArray());
    }

    private static boolean isChecksumValid(String barcode) {
        return UPCEANLogicImpl.calcChecksum(barcode.substring(0, barcode.length() - 1)) == barcode.charAt(barcode.length() - 1);
    }

    public enum EANBarcodeType {
        INVALID, EAN8, EAN13
    }
}
