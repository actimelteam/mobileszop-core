package zpi.adminwebshop.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zpi.adminwebshop.product.Exception.NoSuchProductException;
import zpi.adminwebshop.product.dto.ProductDto;
import zpi.adminwebshop.product.dto.ProductDtoMapper;
import zpi.adminwebshop.user.Exception.InvalidTokenException;
import zpi.adminwebshop.user.Exception.InvalidUserException;
import zpi.adminwebshop.user.User;
import zpi.adminwebshop.user.UserService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/products")
public class ProductApiController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ProductService productService;
    private final ProductDtoMapper productDtoMapper;
    private UserService userService;

    public ProductApiController(ProductService productService, ProductDtoMapper productDtoMapper, UserService userService) {
        this.productService = productService;
        this.productDtoMapper = productDtoMapper;
        this.userService = userService;
    }

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProductDto> findAllProducts() {
        return productService.findAll().stream().map(productDtoMapper::productToDto).collect(Collectors.toList());
    }

    @GetMapping("/product/{id}/photo")
    @ResponseBody
    public ResponseEntity<byte[]> getProductImage(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(productService.getImage(id));
        } catch (NoSuchProductException e) {
            logger.warn("No product with id " + e.getProductId() + " found", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found".getBytes());
        }
    }

    @GetMapping("/afterUpdateDate/{updateDate}")
    @ResponseBody
    public ResponseEntity<List<ProductDto>> getProductsAfterGivenDate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime updateDate) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getProductsAfterGivenDate(updateDate)
                .stream().map(productDtoMapper::productToDto).collect(Collectors.toList()));
    }

    @GetMapping("/popular/{topAmount}")
    @ResponseBody
    public ResponseEntity<List<Long>> getTopPopularProducts(@PathVariable int topAmount) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(productService.getMostPopularProductsForAll(topAmount)
                        .stream()
                        .map(Product::getId)
                        .collect(Collectors.toList()));
    }

    @GetMapping("/popular/{userToken}/{topAmount}")
    @ResponseBody
    public ResponseEntity<List<Long>> getTopPopularProductsForGivenUser(@PathVariable int topAmount, @PathVariable String userToken) {
        User user;
        try {
            user = userService.getUserByGoogleToken(userToken);
        } catch (InvalidTokenException | IOException | GeneralSecurityException e) {
            logger.warn("Token is wrong or not passed", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ArrayList<>());
        } catch (InvalidUserException e) {
            logger.info("Authentication failed during fetching most popular products for user", e);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(productService.getMostPopularProductsForGivenUser(topAmount, user)
                        .stream()
                        .map(Product::getId)
                        .collect(Collectors.toList()));
    }
}
