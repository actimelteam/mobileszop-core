package zpi.adminwebshop.shop;


import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class ShopService {

    private ShopRepository shopRepository;

    public ShopService(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    public Shop getShop() {
        List<Shop> shops = shopRepository.findAll();
        if (!shops.isEmpty()) {
            return shops.get(0);
        }
        return new Shop();
    }

    public void save(Shop shop) {
        shop.setLastUpdateDate(new Date());
        shopRepository.save(shop);
    }
}
