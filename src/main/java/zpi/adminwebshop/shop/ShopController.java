package zpi.adminwebshop.shop;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/shop")
public class ShopController {
    private static final int MESSAGE_SHOP_INFO_SAVED = 0;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("")
    public String showShopSettings(Model model) {
        model.addAttribute("shop", shopService.getShop());
        return "shop/shop_info";
    }

    @PostMapping("/save")
    public String updateSettings(@ModelAttribute Shop shop, RedirectAttributes redirectAttributes) {
        shopService.save(shop);
        redirectAttributes.addFlashAttribute("messageCode", MESSAGE_SHOP_INFO_SAVED);
        return "redirect:/shop";
    }
}
