package zpi.adminwebshop.shop.dto;

import org.springframework.stereotype.Component;
import zpi.adminwebshop.shop.Shop;

@Component
public class ShopDtoMapper {


    public ShopInfoDto shopToShopInfoDto(Shop shop) {
        ShopInfoDto shopInfoDto = new ShopInfoDto();
        shopInfoDto.setName(shop.getName())
                .setAddress(shop.getAddress())
                .setAdditionalInfo(shop.getAdditionalInfo())
                .setEndOrderingTime(shop.getEndOrderingTime())
                .setStartCollectingTime(shop.getStartCollectingTime())
                .setEndCollectingTime(shop.getEndCollectingTime());
        return shopInfoDto;
    }
}
