package zpi.adminwebshop.shop.dto;

public class ShopInfoDto {
    private String name;
    private String address;
    private String additionalInfo;
    private String startCollectingTime;
    private String endCollectingTime;
    private String endOrderingTime;

    public String getName() {
        return name;
    }

    public ShopInfoDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public ShopInfoDto setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public ShopInfoDto setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public String getStartCollectingTime() {
        return startCollectingTime;
    }

    public ShopInfoDto setStartCollectingTime(String startCollectingTime) {
        this.startCollectingTime = startCollectingTime;
        return this;
    }

    public String getEndCollectingTime() {
        return endCollectingTime;
    }

    public ShopInfoDto setEndCollectingTime(String endCollectingTime) {
        this.endCollectingTime = endCollectingTime;
        return this;
    }

    public String getEndOrderingTime() {
        return endOrderingTime;
    }

    public ShopInfoDto setEndOrderingTime(String endOrderingTime) {
        this.endOrderingTime = endOrderingTime;
        return this;
    }
}
