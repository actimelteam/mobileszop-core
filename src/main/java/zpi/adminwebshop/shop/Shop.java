package zpi.adminwebshop.shop;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "shop")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Size(max = 5)
    @NotNull
    private String startCollectingTime;

    @NotNull
    @Size(max = 5)
    private String endCollectingTime;

    @NotNull
    @Size(max = 5)
    private String endOrderingTime;

    private Date lastUpdateDate;

    private String address;

    private String additionalInfo;


    public Long getId() {
        return id;
    }

    public Shop setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Shop setName(String name) {
        this.name = name;
        return this;
    }

    public String getStartCollectingTime() {
        return startCollectingTime;
    }

    public Shop setStartCollectingTime(String startCollectingTime) {
        this.startCollectingTime = startCollectingTime;
        return this;
    }

    public String getEndCollectingTime() {
        return endCollectingTime;
    }

    public Shop setEndCollectingTime(String endCollectingTime) {
        this.endCollectingTime = endCollectingTime;
        return this;
    }

    public String getEndOrderingTime() {
        return endOrderingTime;
    }

    public Shop setEndOrderingTime(String endOrderingTime) {
        this.endOrderingTime = endOrderingTime;
        return this;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public Shop setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Shop setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public Shop setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }
}
