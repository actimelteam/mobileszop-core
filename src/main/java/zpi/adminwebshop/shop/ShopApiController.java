package zpi.adminwebshop.shop;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import zpi.adminwebshop.shop.dto.ShopDtoMapper;
import zpi.adminwebshop.shop.dto.ShopInfoDto;

@RestController
@RequestMapping("/api/shop")
public class ShopApiController {

    private ShopService shopService;
    private ShopDtoMapper shopDtoMapper;

    public ShopApiController(ShopService shopService, ShopDtoMapper shopDtoMapper) {
        this.shopService = shopService;
        this.shopDtoMapper = shopDtoMapper;
    }


    @GetMapping("/info")
    @ResponseBody
    public ResponseEntity<ShopInfoDto> getShopInfo() {
        return ResponseEntity.status(HttpStatus.OK).body(shopDtoMapper.shopToShopInfoDto(shopService.getShop()));
    }
}
