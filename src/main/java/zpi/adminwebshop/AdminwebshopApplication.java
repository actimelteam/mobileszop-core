package zpi.adminwebshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminwebshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminwebshopApplication.class, args);
	}
}
