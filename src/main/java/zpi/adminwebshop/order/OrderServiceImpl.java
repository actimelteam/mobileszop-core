package zpi.adminwebshop.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zpi.adminwebshop.order.Exception.NoSuchOrderException;
import zpi.adminwebshop.order.dto.OrderDtoMapper;
import zpi.adminwebshop.user.User;

import java.util.List;
import java.util.Set;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private OrderPositionRepository orderPositionRepository;
    private OrderDtoMapper orderDtoMapper;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderPositionRepository orderPositionRepository, OrderDtoMapper orderDtoMapper) {
        this.orderRepository = orderRepository;
        this.orderPositionRepository = orderPositionRepository;
        this.orderDtoMapper = orderDtoMapper;
    }

    @Override
    public Set<Order> findOrdersByStatus(Order.OrderStatus status) {
        return orderRepository.findByStatusOrderByPlannedCollectionDate(status);
    }

    @Override
    public Page<Order> findOrdersByStatus(Order.OrderStatus status, Pageable pageable) {
        return orderRepository.findByStatusOrderByPlannedCollectionDate(status, pageable);
    }

    @Override
    public Set<Order> findAll() {
        return orderRepository.findAllByOrderByPlannedCollectionDateDesc();
    }

    @Override
    public Page<Order> findAll(Pageable pageable) {
        return orderRepository.findAllByOrderByPlannedCollectionDateDesc(pageable);
    }

    @Override
    public Order findById(Long id) throws NoSuchOrderException {
        return orderRepository.findById(id).orElseThrow(() -> new NoSuchOrderException(id));
    }

    @Override
    public void setOrderStatus(Long id, Order.OrderStatus status) {
        orderRepository.setOrderStatus(id, status);
    }

    @Override
    public Order.OrderStatus getStatusFromString(String statusName) {
        Order.OrderStatus result;
        switch (statusName) {
            case "submitted":
                result = Order.OrderStatus.SUBMITTED;
                break;
            case "ready_to_collect":
                result = Order.OrderStatus.READY_TO_COLLECT;
                break;
            case "realised":
                result = Order.OrderStatus.REALISED;
                break;
            case "canceled":
                result = Order.OrderStatus.CANCELED;
                break;
            default:
                result = null;
        }
        return result;
    }

    @Override
    public void saveOrder(Order order) {
        orderRepository.save(order);
    }

    @Override
    public long orderPositionCountByProductId(long productId) {
        return orderPositionRepository.countByProduct_Id(productId);
    }

    @Override
    public List<Order> findByUser(User user) {
        return orderRepository.findByUser(user);
    }
}
