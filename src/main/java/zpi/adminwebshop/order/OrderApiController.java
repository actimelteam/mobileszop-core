package zpi.adminwebshop.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zpi.adminwebshop.order.dto.CreateOrderDto;
import zpi.adminwebshop.order.dto.OrderDto;
import zpi.adminwebshop.order.dto.OrderDtoMapper;
import zpi.adminwebshop.product.Exception.NoSuchProductException;
import zpi.adminwebshop.user.Exception.InvalidTokenException;
import zpi.adminwebshop.user.Exception.InvalidUserException;
import zpi.adminwebshop.user.User;
import zpi.adminwebshop.user.UserService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
public class OrderApiController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private OrderService orderService;
    private OrderDtoMapper orderDtoMapper;
    private UserService userService;

    public OrderApiController(OrderService orderService, OrderDtoMapper orderDtoMapper, UserService userService) {
        this.orderService = orderService;
        this.orderDtoMapper = orderDtoMapper;
        this.userService = userService;
    }

    @PostMapping("/addOrder")
    @ResponseBody
    public ResponseEntity addOrder(@RequestBody CreateOrderDto createOrderDto) {
        Order order;
        try {
            order = orderDtoMapper.createOrderDtoToOrder(createOrderDto);
        } catch (InvalidUserException e) {
            logger.info("Authentication failed during order submitting");
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        } catch (InvalidTokenException | IOException | GeneralSecurityException e) {
            logger.warn("Invalid token for submitted order");
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NoSuchProductException e) {
            logger.error("Product with id " + e.getProductId() + " not found in order for: " + createOrderDto.getOrderPositionsDtoSet().toString(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid Product");
        }
        orderService.saveOrder(order);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{userToken}")
    @ResponseBody
    public ResponseEntity<List<OrderDto>> getOrdersByUser(@PathVariable String userToken) {
        User user;
        try {
            user = userService.getUserByGoogleTokenAndSaveIfNotExist(userToken);
        } catch (InvalidTokenException | IOException | GeneralSecurityException e) {
            logger.warn("Invalid token for getting orders for user", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (InvalidUserException e) {
            logger.info("Authentication failed during getting orders for user");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(orderService.findByUser(user).stream().map(order -> orderDtoMapper.orderToOrderDto(order)).collect(Collectors.toList()), HttpStatus.OK);
    }
}
