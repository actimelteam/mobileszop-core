package zpi.adminwebshop.order;

import zpi.adminwebshop.product.Product;

import java.io.Serializable;
import java.util.Objects;

public class OrderPositionPrimaryKey implements Serializable {
    private Order order;
    private Product product;

    public OrderPositionPrimaryKey() {
    }

    public OrderPositionPrimaryKey(Order order, Product product) {
        this.order = order;
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderPositionPrimaryKey)) return false;
        OrderPositionPrimaryKey that = (OrderPositionPrimaryKey) o;
        return Objects.equals(order, that.order) &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order, product);
    }
}
