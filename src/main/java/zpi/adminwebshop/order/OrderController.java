package zpi.adminwebshop.order;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zpi.adminwebshop.error.Error;
import zpi.adminwebshop.order.Exception.NoSuchOrderException;
import zpi.adminwebshop.order.dto.OrderPositionDtoMapper;
import zpi.adminwebshop.order.dto.OrderPositionViewDto;
import zpi.adminwebshop.product.Exception.InvalidEANBarcodeException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController {

    private static final int PRODUCT_STATUS_SAVED = 0;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private OrderService orderService;
    private final OrderPositionDtoMapper orderPositionDtoMapper;

    @Autowired
    public OrderController(OrderService orderService, OrderPositionDtoMapper orderPositionDtoMapper) {
        this.orderService = orderService;
        this.orderPositionDtoMapper = orderPositionDtoMapper;
    }

    @GetMapping("/")
    public String redirectToOrders() {
        return "redirect:/orders/submitted";
    }

    @GetMapping("/orders")
    public String showAllOrders(Model model, Pageable pageable) {
        model.addAttribute("orders", orderService.findAll(pageable));
        return "order/orders";
    }

    @GetMapping("/orders/{orderStatus}")
    public String showOrdersByStatus(@PathVariable(required = false) String orderStatus, Model model, Pageable pageable) {
        Order.OrderStatus status = orderService.getStatusFromString(orderStatus);
        model.addAttribute("orders", orderService.findOrdersByStatus(status, pageable));
        return "order/orders";
    }

    @GetMapping("/orders/order/{id}")
    public ModelAndView showOrder(ModelAndView modelAndView, @PathVariable long id) {
        modelAndView.setViewName("order/order-details");
        Order order;
        try {
            order = orderService.findById(id);
        } catch (NoSuchOrderException e) {
            logger.warn("Order with id " + e.getOrderId() + " not found", e);
            modelAndView.setViewName("error");
            modelAndView.addObject("errorCode", Error.NO_SUCH_ORDER);
            return modelAndView;
        }
        List<OrderPositions> orderPositions = new ArrayList<>(order.getOrderPositionsSet());
        orderPositions.sort((o1, o2) -> o1.getProduct().getName().compareTo(o2.getProduct().getName()));

        List<OrderPositionViewDto> positionsView = null;
        try {
            positionsView = orderPositionDtoMapper.mapOrderPositionsToViewDto(orderPositions);
        } catch (IOException e) {
            logger.error("error during converting barcode number to image", e);
        } catch (InvalidEANBarcodeException e) {
            logger.error("invalid barcode format detected during converting barcode number to image", e);
        }

        modelAndView.addObject("order", order);
        modelAndView.addObject("orderPositions", positionsView);
        return modelAndView;
    }

    @PostMapping("orders/save-status")
    public ModelAndView saveNewOrderStatus(ModelAndView modelAndView,
                                           HttpServletRequest request,
                                           RedirectAttributes redirectAttributes) {
        long id = Long.parseLong(request.getParameter("order-id"));
        Order.OrderStatus newStatus = Order.OrderStatus.valueOf(request.getParameter("order-status"));
        orderService.setOrderStatus(id, newStatus);
        redirectAttributes.addFlashAttribute("messageCode", PRODUCT_STATUS_SAVED);
        modelAndView.setViewName("redirect:" + request.getHeader("Referer"));
        return modelAndView;
    }

    @GetMapping("orders/orderPositionCountWithProduct/{productId}")
    public @ResponseBody Long getOrderPositionCountWithProduct(@PathVariable Long productId) {
        return orderService.orderPositionCountByProductId(productId);
    }
}
