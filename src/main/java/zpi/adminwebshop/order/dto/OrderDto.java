package zpi.adminwebshop.order.dto;

import zpi.adminwebshop.order.Order;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Set;

public class OrderDto {

    private long id;

    private LocalDateTime submissionDate;

    private LocalDateTime plannedCollectionDate;

    private Order.OrderStatus status;


    private Set<OrderPositionsDto> orderPositionsSet;

    public OrderDto(long id, Date submissionDate, Date plannedCollectionDate, Order.OrderStatus status, Set<OrderPositionsDto> orderPositionsSet) {
        this.id = id;
        this.submissionDate = submissionDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.plannedCollectionDate = plannedCollectionDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.status = status;
        this.orderPositionsSet = orderPositionsSet;
    }

    public OrderDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getSubmissionDate() {
        return submissionDate;
    }

    public OrderDto setSubmissionDate(LocalDateTime submissionDate) {
        this.submissionDate = submissionDate;
        return this;
    }

    public LocalDateTime getPlannedCollectionDate() {
        return plannedCollectionDate;
    }

    public OrderDto setPlannedCollectionDate(LocalDateTime plannedCollectionDate) {
        this.plannedCollectionDate = plannedCollectionDate;
        return this;
    }

    public Order.OrderStatus getStatus() {
        return status;
    }

    public void setStatus(Order.OrderStatus status) {
        this.status = status;
    }

    public Set<OrderPositionsDto> getOrderPositionsSet() {
        return orderPositionsSet;
    }

    public void setOrderPositionsSet(Set<OrderPositionsDto> orderPositionsSet) {
        this.orderPositionsSet = orderPositionsSet;
    }
}
