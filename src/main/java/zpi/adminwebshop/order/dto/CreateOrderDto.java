package zpi.adminwebshop.order.dto;

import java.util.Set;

public class CreateOrderDto {
    private String userToken;
    private Set<OrderPositionsDto> orderPositionsDtoSet;
    private String submissionDate;
    private String plannedCollectionDate;

    public CreateOrderDto() {
    }

    public String getUserToken() {
        return userToken;
    }

    public CreateOrderDto setUserToken(String userToken) {
        this.userToken = userToken;
        return this;
    }

    public Set<OrderPositionsDto> getOrderPositionsDtoSet() {
        return orderPositionsDtoSet;
    }

    public CreateOrderDto setOrderPositionsDtoSet(Set<OrderPositionsDto> orderPositionsDtoSet) {
        this.orderPositionsDtoSet = orderPositionsDtoSet;
        return this;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public CreateOrderDto setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
        return this;
    }

    public String getPlannedCollectionDate() {
        return plannedCollectionDate;
    }

    public CreateOrderDto setPlannedCollectionDate(String plannedCollectionDate) {
        this.plannedCollectionDate = plannedCollectionDate;
        return this;
    }
}
