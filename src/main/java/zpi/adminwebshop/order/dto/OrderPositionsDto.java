package zpi.adminwebshop.order.dto;

public class OrderPositionsDto {

    private long productId;
    private double amount;
    private double unitPrice;

    public OrderPositionsDto(long productId, double amount, double unitPrice) {
        this.productId = productId;
        this.amount = amount;
        this.unitPrice = unitPrice;
    }

    public OrderPositionsDto() {
    }

    public long getProductId() {
        return productId;
    }

    public OrderPositionsDto setProductId(long productId) {
        this.productId = productId;
        return this;
    }

    public double getAmount() {
        return amount;
    }

    public OrderPositionsDto setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public OrderPositionsDto setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "productId=" + productId +
                ", amount=" + amount +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
