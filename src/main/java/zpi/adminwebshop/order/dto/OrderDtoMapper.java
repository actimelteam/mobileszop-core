package zpi.adminwebshop.order.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import zpi.adminwebshop.order.Order;
import zpi.adminwebshop.order.OrderPositions;
import zpi.adminwebshop.product.Exception.NoSuchProductException;
import zpi.adminwebshop.product.Product;
import zpi.adminwebshop.product.ProductService;
import zpi.adminwebshop.user.Exception.InvalidTokenException;
import zpi.adminwebshop.user.Exception.InvalidUserException;
import zpi.adminwebshop.user.User;
import zpi.adminwebshop.user.UserService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class OrderDtoMapper {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ProductService productService;
    private UserService userService;

    public OrderDtoMapper(ProductService productService, UserService userService) {
        this.productService = productService;
        this.userService = userService;
    }

    public OrderDto orderToOrderDto(Order order) {

        Set<OrderPositionsDto> orderPositionsDtoSet = new HashSet<>();

        for (OrderPositions orderPositions : order.getOrderPositionsSet()) {
            orderPositionsDtoSet.add(new OrderPositionsDto(orderPositions.getProduct().getId(), orderPositions.getAmount(), orderPositions.getPrice()));
        }

        return new OrderDto(order.getId(), order.getSubmissionDate(), order.getPlannedCollectionDate(), order.getStatus(), orderPositionsDtoSet);
    }

    public Order createOrderDtoToOrder(CreateOrderDto createOrderDto) throws GeneralSecurityException, InvalidUserException, IOException, NoSuchProductException, InvalidTokenException {
        User user = userService.getUserByGoogleTokenAndSaveIfNotExist(createOrderDto.getUserToken());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date submissionDate = getDateFromString(createOrderDto.getSubmissionDate(), formatter);
        Date plannedCollectionDate = getDateFromString(createOrderDto.getPlannedCollectionDate(), formatter);

        Order order = new Order();
        order.setSubmissionDate(submissionDate);
        order.setPlannedCollectionDate(plannedCollectionDate);
        order.setUser(user);
        order.setStatus(Order.OrderStatus.SUBMITTED);
        saveOrderPositionsForOrder(createOrderDto.getOrderPositionsDtoSet(), order);
        return order;
    }

    private Date getDateFromString(String dateInString, SimpleDateFormat format) {
        Date date = new Date();
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            logger.error("error during parsing order date", e);
        }
        return date;
    }

    private void saveOrderPositionsForOrder(Set<OrderPositionsDto> orderPositionsDtoSet, Order order) throws NoSuchProductException {
        Set<OrderPositions> orderPositionsSet = new HashSet<>();
        for (OrderPositionsDto orderPositionsDto : orderPositionsDtoSet) {
            OrderPositions orderPositions = new OrderPositions();
            Product product = productService
                    .findById(orderPositionsDto.getProductId())
                    .orElseThrow(() -> new NoSuchProductException(orderPositionsDto.getProductId()));
            orderPositions.setProduct(product);
            orderPositions.setAmount(orderPositionsDto.getAmount());
            orderPositions.setPrice(orderPositionsDto.getUnitPrice());
            orderPositions.setOrder(order);
            orderPositionsSet.add(orderPositions);
        }
        order.setOrderPositionsSet(orderPositionsSet);
    }
}
