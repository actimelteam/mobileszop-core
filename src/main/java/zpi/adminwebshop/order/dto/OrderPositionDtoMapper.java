package zpi.adminwebshop.order.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zpi.adminwebshop.order.OrderPositions;
import zpi.adminwebshop.product.EANBarcodeService;
import zpi.adminwebshop.product.Exception.InvalidEANBarcodeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderPositionDtoMapper {
    private final EANBarcodeService barcodeService;

    @Autowired
    public OrderPositionDtoMapper(EANBarcodeService barcodeService) {
        this.barcodeService = barcodeService;
    }

    public List<OrderPositionViewDto> mapOrderPositionsToViewDto(List<OrderPositions> positions) throws IOException, InvalidEANBarcodeException {
        List<OrderPositionViewDto> mappedPositions = new ArrayList<>(positions.size());
        for (OrderPositions orderPosition : positions) {
            OrderPositionViewDto positionDto = new OrderPositionViewDto();
            positionDto.setAmount(orderPosition.getAmount());
            if (orderPosition.getProduct().getBarcode() != null) {
                positionDto.setBarcodeImage(barcodeService.getBarcodeImageBase64(orderPosition.getProduct().getBarcode()));
            }
            positionDto.setPrice(orderPosition.getPrice());
            positionDto.setProductName(orderPosition.getProduct().getName());
            positionDto.setStatus(orderPosition.getProduct().getStatus());
            positionDto.setUnit(orderPosition.getProduct().getUnit());
            mappedPositions.add(positionDto);
        }
        return mappedPositions;
    }
}
