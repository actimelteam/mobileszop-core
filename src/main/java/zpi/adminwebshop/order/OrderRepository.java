package zpi.adminwebshop.order;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zpi.adminwebshop.user.User;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    Set<Order> findByStatusOrderByPlannedCollectionDate(Order.OrderStatus orderStatus);

    Page<Order> findByStatusOrderByPlannedCollectionDate(Order.@NotNull OrderStatus status, Pageable pageable);
    Set<Order> findAllByOrderByPlannedCollectionDateDesc();

    Page<Order> findAllByOrderByPlannedCollectionDateDesc(Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Order o set o.status = ?2 where o.id = ?1")
    void setOrderStatus(Long id,Order.OrderStatus status);

    List<Order> findByUser(User userName);
}
