package zpi.adminwebshop.order;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import zpi.adminwebshop.order.Exception.NoSuchOrderException;
import zpi.adminwebshop.user.User;

import java.util.List;
import java.util.Set;

public interface OrderService {

    Set<Order> findOrdersByStatus(Order.OrderStatus status);

    Page<Order> findOrdersByStatus(Order.OrderStatus status, Pageable pageable);

    Set<Order> findAll();

    Page<Order> findAll(Pageable pageable);

    Order findById(Long id) throws NoSuchOrderException;

    void setOrderStatus(Long id, Order.OrderStatus status);

    Order.OrderStatus getStatusFromString(String statusName);

    void saveOrder(Order order);
    long orderPositionCountByProductId(long productId);

    List<Order> findByUser(User user);
}
