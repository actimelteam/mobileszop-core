package zpi.adminwebshop.order.Exception;

public class NoSuchOrderException extends Exception {
    private long orderId;

    public NoSuchOrderException() {
    }

    public NoSuchOrderException(long orderId) {
        this.orderId = orderId;
    }

    public long getOrderId() {
        return orderId;
    }
}
