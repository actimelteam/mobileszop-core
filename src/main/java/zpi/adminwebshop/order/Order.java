package zpi.adminwebshop.order;


import zpi.adminwebshop.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date submissionDate;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date plannedCollectionDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderPositions> orderPositionsSet;

    public enum OrderStatus{
        SUBMITTED, READY_TO_COLLECT, REALISED, CANCELED
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    public Date getPlannedCollectionDate() {
        return plannedCollectionDate;
    }

    public void setPlannedCollectionDate(Date plannedCollectionDate) {
        this.plannedCollectionDate = plannedCollectionDate;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Set<OrderPositions> getOrderPositionsSet() {
        return orderPositionsSet;
    }

    public void setOrderPositionsSet(Set<OrderPositions> orderPositionsSet) {
        this.orderPositionsSet = orderPositionsSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (!user.equals(order.user)) return false;
        if (!submissionDate.equals(order.submissionDate)) return false;
        if (!plannedCollectionDate.equals(order.plannedCollectionDate)) return false;
        return status == order.status;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + user.hashCode();
        result = 31 * result + submissionDate.hashCode();
        result = 31 * result + plannedCollectionDate.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }
}
