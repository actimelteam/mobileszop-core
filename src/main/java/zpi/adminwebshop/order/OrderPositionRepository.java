package zpi.adminwebshop.order;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderPositionRepository extends JpaRepository<OrderPositions, OrderPositionPrimaryKey> {
    long countByProduct_Id(long product_id);
}
