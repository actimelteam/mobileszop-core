package zpi.adminwebshop.category;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findByParentCategoryIsNull();
    long countByParentCategoryId(long id);

    List<Category> findByNameContaining(String name);
}
