package zpi.adminwebshop.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import zpi.adminwebshop.product.ProductRepository;

import java.util.*;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<Category> getForbiddenParentCategories(Category category) {
        if(category != null) {
            List<Category> forbiddenParentCategories = new ArrayList<>();
            for (Category c : category.getSubcategories()) {
                forbiddenParentCategories.add(c);
                forbiddenParentCategories.addAll(getForbiddenParentCategories(c));
            }
            return forbiddenParentCategories;
        }
        return null;
    }

    @Override
    public List<Category> getCategoriesWithoutParentCategory() {
        return categoryRepository.findByParentCategoryIsNull();
    }

    @Override
    public Map<Long, List<Category>> getForbiddenParentCategoriesForCategories(List<Category> categories) {
        Map<Long, List<Category>> forbiddenParentCategories = new HashMap<>();
        for(Category category : categories) {
            forbiddenParentCategories.put(category.getId(), getForbiddenParentCategories(category));
        }
        return forbiddenParentCategories;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public boolean deleteCategoryById(long categoryId) throws EmptyResultDataAccessException {
        long productCount = productRepository.countByCategory_Id(categoryId);
        long subcategoriesCount = categoryRepository.countByParentCategoryId(categoryId);
        if (productCount == 0 && subcategoriesCount == 0) {
            categoryRepository.deleteById(categoryId);
            return true;
        }
        return  false;
    }

    @Override
    public List<Category> getCategoriesByNameContaining(String name) {
        return categoryRepository.findByNameContaining(name);
    }
}
