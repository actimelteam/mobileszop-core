package zpi.adminwebshop.category;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface CategoryService {
    List<Category> getForbiddenParentCategories(Category category);
    List<Category> getCategoriesWithoutParentCategory();
    Map<Long, List<Category>> getForbiddenParentCategoriesForCategories(List<Category> categories);
    List<Category> findAll();
    Optional<Category> findById(Long id);
    boolean deleteCategoryById(long categoryId);

    List<Category> getCategoriesByNameContaining(String name);
}
