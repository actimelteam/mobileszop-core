package zpi.adminwebshop.category.dto;

import java.util.Set;

public class CategoryViewDto {
    private long id;
    private String name;
    private String fullName;
    private Set<CategoryViewDto> subcategories;
    private Set<CategoryViewDto> forbiddenParentCategories;
    private CategoryViewDto parentCategory;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<CategoryViewDto> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(Set<CategoryViewDto> subcategories) {
        this.subcategories = subcategories;
    }

    public Set<CategoryViewDto> getForbiddenParentCategories() {
        return forbiddenParentCategories;
    }

    public void setForbiddenParentCategories(Set<CategoryViewDto> forbiddenParentCategories) {
        this.forbiddenParentCategories = forbiddenParentCategories;
    }

    public CategoryViewDto getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(CategoryViewDto parentCategory) {
        this.parentCategory = parentCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CategoryViewDto)) return false;
        CategoryViewDto that = (CategoryViewDto) o;
        return id == that.id;
    }
}
