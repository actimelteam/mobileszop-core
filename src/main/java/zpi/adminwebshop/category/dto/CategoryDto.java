package zpi.adminwebshop.category.dto;

import zpi.adminwebshop.category.Category;

import java.util.HashSet;
import java.util.Set;

public class CategoryDto {

    private long id;
    private String name;
    private Set<CategoryDto> subcategories;

    public CategoryDto(long id, String name, Set<Category> subcategories) {
        this.id = id;
        this.name = name;
        this.subcategories = new HashSet<>();

        if (subcategories != null && !subcategories.isEmpty()) {
            for (Category category : subcategories) {
                this.subcategories.add(new CategoryDto(category.getId(), category.getName(), category.getSubcategories()));
            }
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CategoryDto> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(Set<CategoryDto> subcategories) {
        this.subcategories = subcategories;
    }
}
