package zpi.adminwebshop.category.dto;

import org.springframework.stereotype.Component;
import zpi.adminwebshop.category.Category;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
public class CategoryDtoMapper {
    public static final String PATH_SEPARATOR = " / ";

    public CategoryDto categoriesToDto(Set<Category> categories) {
        return new CategoryDto(0, "Root", categories);
    }
    public List<CategorySimpleDto> deepMapCategoriesToSimpleDto(List<Category> rootCategories) {
        List<CategorySimpleDto> categories = new LinkedList<>();
        for (Category category : rootCategories) {
            categoryToSimpleDto(category ,"", categories);
        }
        return categories;
    }

    private void categoryToSimpleDto(Category category, String path, List<CategorySimpleDto> categories) {
        if (category.getParentCategory() != null)
            path += PATH_SEPARATOR;
        path += category.getName();
        CategorySimpleDto simpleCategory = new CategorySimpleDto(category.getId(), path);
        categories.add(simpleCategory);
        for (Category subcategory : category.getSubcategories()) {
            categoryToSimpleDto(subcategory, path, categories);
        }
    }

    public static List<CategoryViewDto> deepMapCategoryToViewDto(List<Category> rootCategories) {
        List<CategoryViewDto> categories = new LinkedList<>();
        for (Category category : rootCategories) {
            categories.add(deepMapCategoryToViewDto(category, null));
        }
        return categories;
    }

    private static CategoryViewDto deepMapCategoryToViewDto(Category category, String fullPath) {
        CategoryViewDto categoryView = new CategoryViewDto();
        categoryView.setId(category.getId());
        categoryView.setName(category.getName());
        Set<CategoryViewDto> forbiddenCategories = new HashSet<>();
        Set<CategoryViewDto> subcategories = new HashSet<>();
        fullPath = fullPath == null ? category.getName() : fullPath + PATH_SEPARATOR + category.getName();
        categoryView.setFullName(fullPath);
        for (Category c : category.getSubcategories()) {
            CategoryViewDto subcategoryView = deepMapCategoryToViewDto(c, fullPath);
            subcategories.add(subcategoryView);
            subcategoryView.setParentCategory(categoryView);
            forbiddenCategories.add(subcategoryView);
            forbiddenCategories.addAll(subcategoryView.getForbiddenParentCategories());
        }
        categoryView.setSubcategories(subcategories);
        categoryView.setForbiddenParentCategories(forbiddenCategories);
        return categoryView;
    }

    public static List<CategoryViewDto> flattenCategoryViewTree(List<CategoryViewDto> rootCategories) {
        List<CategoryViewDto> categories = new LinkedList<>();
        for (CategoryViewDto category : rootCategories) {
            flattenCategoryViewTree(category, categories);
        }
        return  categories;
    }

    private static void flattenCategoryViewTree(CategoryViewDto category, List<CategoryViewDto> categories) {
        categories.add(category);
        for (CategoryViewDto c : category.getSubcategories()) {
            flattenCategoryViewTree(c, categories);
        }
    }

    public static Category categoryViewDtoToCategoryWithoutTree (CategoryViewDto categoryView) {
        Category category = new Category();
        category.setId(categoryView.getId());
        category.setName(categoryView.getName());
        return category;
    }
}
