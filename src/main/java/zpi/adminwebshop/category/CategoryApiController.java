package zpi.adminwebshop.category;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zpi.adminwebshop.category.dto.CategoryDto;
import zpi.adminwebshop.category.dto.CategoryDtoMapper;

import java.util.HashSet;

@RestController
@RequestMapping("/api/categories")
public class CategoryApiController {

    private final CategoryDtoMapper categoryDtoMapper;
    private final CategoryService categoryService;

    public CategoryApiController(CategoryDtoMapper categoryDtoMapper, CategoryService categoryService) {
        this.categoryDtoMapper = categoryDtoMapper;
        this.categoryService = categoryService;
    }

    @GetMapping("")
    public CategoryDto getCategoriesTree() {
        return categoryDtoMapper.categoriesToDto(new HashSet<>(categoryService.getCategoriesWithoutParentCategory()));
    }

}
