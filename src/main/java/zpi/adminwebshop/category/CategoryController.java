package zpi.adminwebshop.category;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zpi.adminwebshop.category.dto.CategoryDtoMapper;
import zpi.adminwebshop.category.dto.CategoryViewDto;
import zpi.adminwebshop.error.Error;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CategoryController {
    private static final int MESSAGE_CATEGORY_ADDED = 0;
    private static final int MESSAGE_CATEGORY_UPDATED = 1;
    private static final int MESSAGE_CATEGORY_DELETED = 2;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final CategoryRepository categoryRepository;
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryRepository categoryRepository, CategoryService categoryService) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public String showCategories(Model model) {
        List<CategoryViewDto> rootCategories = CategoryDtoMapper.deepMapCategoryToViewDto(categoryService.getCategoriesWithoutParentCategory());
        model.addAttribute("categories", rootCategories);
        model.addAttribute("allCategories", CategoryDtoMapper.flattenCategoryViewTree(rootCategories));
        model.addAttribute("newCategory", new CategoryViewDto());
        return "category/categories";
    }

    @PostMapping(value = "/updateCategory")
    public String updateCategory(@ModelAttribute CategoryViewDto categoryView,
                                 HttpServletRequest request,
                                 final RedirectAttributes redirectAttributes) {
        String parentCategoryId = request.getParameter("parentCategoryId");
        Category category = CategoryDtoMapper.categoryViewDtoToCategoryWithoutTree(categoryView);
        Category parent = null;
        if (!parentCategoryId.isEmpty()) {
            parent = categoryRepository.getOne((long) Integer.valueOf(parentCategoryId));
        }
        category.setParentCategory(parent);
        categoryRepository.save(category);
        redirectAttributes.addFlashAttribute("messageCode", MESSAGE_CATEGORY_UPDATED);
        redirectAttributes.addFlashAttribute("lastModifiedCategoryId", category.getId());
        return "redirect:/categories";
    }

    @PostMapping("/addCategory")
    public String addCategory(@ModelAttribute CategoryViewDto categoryView,
                              HttpServletRequest request,
                              final RedirectAttributes redirectAttributes) {
        Category category = CategoryDtoMapper.categoryViewDtoToCategoryWithoutTree(categoryView);
        String parentCategoryId = request.getParameter("parentCategoryId");
        if (!parentCategoryId.isEmpty()) {
            Category parentCategory = new Category();
            parentCategory.setId(Integer.valueOf(parentCategoryId));
            category.setParentCategory(parentCategory);
        }
        categoryRepository.save(category);
        redirectAttributes.addFlashAttribute("messageCode", MESSAGE_CATEGORY_ADDED);
        redirectAttributes.addFlashAttribute("lastModifiedCategoryId", category.getId());
        return "redirect:/categories";
    }

    @PostMapping("/deleteCategory")
    public String deleteCategory(HttpServletRequest request,
                                 final RedirectAttributes redirectAttributes,
                                 Model model) {
        long categoryId = Long.valueOf(request.getParameter("id"));
        Category lastModifiedCategory = categoryRepository.findById(categoryId).map(Category::getParentCategory).orElse(null);
        try {
            if (!categoryService.deleteCategoryById(categoryId)) {
                model.addAttribute("errorCode", Error.CATEGORY_DELETION);
                logger.warn("Deleting category with id = " + categoryId + " which has assigned products");
                return "error";
            }
        } catch (EmptyResultDataAccessException exception) {
            model.addAttribute("errorCode", Error.CATEGORY_DELETION);
            logger.warn("Deleting category with id = " + categoryId + " which not exists");
            return "error";
        }
        redirectAttributes.addFlashAttribute("messageCode", MESSAGE_CATEGORY_DELETED);
        redirectAttributes.addFlashAttribute("lastModifiedCategoryId", lastModifiedCategory != null ? lastModifiedCategory.getId() : null);
        return "redirect:/categories";
    }
}
