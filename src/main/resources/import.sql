

INSERT INTO categories(id, name, parent_category_id) VALUES (1, "Pieczywo", NULL);
INSERT INTO categories(id, name, parent_category_id) VALUES (2, "Nabial", NULL);
INSERT INTO categories(id, name, parent_category_id) VALUES (3, "Sery", 2);
INSERT INTO categories(id, name, parent_category_id) VALUES (4, "Maslo", 2);
INSERT INTO categories (id, name, parent_category_id) VALUES (5, "Mleko", 2);
INSERT INTO categories (id, name, parent_category_id) VALUES (6, "Biale", 3);
INSERT INTO categories (id, name, parent_category_id) VALUES (7, "Zolte", 4);
INSERT INTO categories (id, name, parent_category_id) VALUES (8, "Kategoria", 4);


INSERT INTO products (id, name, price, description, unit, status, category_id)VALUES (1, 'Chleb pszenny 300g', 2.30, 'Chlep pszenny z Twojej lokalnej piekarni', 'PIECE', 'AVAILABLE', 1);
INSERT INTO products (id, name, price, description, unit, status, category_id)VALUES (2, 'Chleb zytni 600g', 3.00, 'Chleb zytni z ziarnami', 'PIECE', 'AVAILABLE', 1);
INSERT INTO products (id, name, price, description, unit, status, category_id)VALUES (3, 'Mleko UHT 1 litr', 2.50, 'Mleko UHT', 'LITER', 'AVAILABLE', 5);
INSERT INTO products (id, name, price, description, unit, status, category_id)VALUES (4, 'Ser Żólty Babuni 400g', 3.95, 'Ser Zolty', 'PIECE', 'UNAVAILABLE', 6);
INSERT INTO products (id, name, price, description, unit, status, category_id) VALUES(5, 'Maslo oselka 300g', 3.95, 'Super maslo oselka produkowane w najznamiennitszych mleczarniach', 'PIECE','AVAILABLE', 4);
INSERT INTO products (id, name, price, description, unit, status, category_id) VALUES(6, 'Ser królewski', 24.95, 'Ser królewski marki Twój ser', 'KILOGRAM','AVAILABLE', 6);


INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (0, 2, '2018-03-10 14:29:36', '2018-03-10 18:00:00', 'SUBMITTED');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (1, 2, '2018-03-05 12:10:27', '2018-03-05 17:30:00', 'REALISED');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (2, 2, '2018-03-01 17:30:21', '2018-03-01 18:00:00', 'REALISED');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (3, 2, '2018-02-19 12:56:41', '2018-02-19 16:00:00', 'CANCELED');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (4, 3, '2018-03-10 10:30:36', '2018-03-10 12:30:00', 'SUBMITTED');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (5, 3, '2018-04-20 18:30:36', '2018-03-10 12:30:00', 'READY_TO_COLLECT');
INSERT INTO orders (id, user_id, submission_date, planned_collection_date, status)VALUES (6, 3, '2018-04-19 12:35:36', '2018-03-10 12:30:00', 'SUBMITTED');

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (0, 1, 3, 4.5);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (0, 2, 13, 2.45);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (0, 3, 1, 3.99);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (0, 4, 1, 4.6);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (0, 6, 1.7, 4.6);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (1, 2, 1, 5.4);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (1, 5, 1, 2.1);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (2, 3, 1, 10.2);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (2, 4, 2, 1.45);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (3, 1, 2, 3.45);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (3, 5, 1, 3.78);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (4, 2, 1, 8.98);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (4, 3, 1, 0.5);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (5, 1, 3, 4.5);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (5, 2, 13, 2.45);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (5, 3, 1, 3.99);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (5, 4, 1, 4.6);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (5, 6, 1.7, 4.6);

INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (6, 1, 3, 4.5);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (6, 2, 13, 2.45);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (6, 3, 1, 3.99);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (6, 4, 1, 4.6);
INSERT INTO order_positions (order_id, product_id, amount, price) VALUES (6, 6, 1.7, 4.6);