function getProductCountFromCategory(categoryId) {
    return new Promise(function (resolve, reject) {
        jQuery.get("http://" + window.location.host + "/products/productCount/" + categoryId, function (productCount) {
            resolve(productCount);
        });
    });
}

function getOrderPositionCountWithProduct(productId) {
    return new Promise(function (resolve, reject) {
        jQuery.get("http://" + window.location.host + "/orders/orderPositionCountWithProduct/" + productId, function (orderPositionCount) {
            resolve(orderPositionCount);
        });
    });
}