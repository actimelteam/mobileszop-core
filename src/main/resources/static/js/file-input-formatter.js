$( document ).ready(function() {
    function autoLoadProductImage() {
        let productPhotoInput = $('.fileInput input[type="file"]');
        let productImage = $('#photoOfProduct');
        productImage.on('load', function () {
            productImage.show();
        });
        autoLoadImage(productPhotoInput, productImage);
    }

    function autoLoadImage(fileInput, image) {
        fileInput.change(function () {
            if (this.files && this.files[0]) {
                let fileReader = new FileReader();

                fileReader.onload = function (event) {
                    image.attr('src', event.target.result);
                };
                fileReader.readAsDataURL(this.files[0]);
            }
        });
    }

  $('.fileInput button').click(function () {
      let fileInput = $('.fileInput input[type="file"]');
      let fileNameInput = $('.fileInput input[type="text"]');
      fileInput.change(function () {
          let filePath = fileInput.val();
          fileNameInput.val(filePath.substr(filePath.lastIndexOf('\\') + 1));
      });
      fileInput.trigger("click");
  });

    autoLoadProductImage();
});