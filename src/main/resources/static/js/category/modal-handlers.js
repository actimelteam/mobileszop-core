$( document ).ready(function() {
    const DELETE_CATEGORY = 'Usuń kategorię';
    const PRODUCTS_AMOUNT_IN_CATEGORY = 'Liczba produktów przypisanych do kategorii';
    const CATEGORY_DELETION_WARNING = 'Nie można usunąć kategorii do której przypisano produkty.';
    const ADD_CATEGORY_TO = 'Dodaj kategorię do';
    const DELETE_PRODUCT = 'Usuń produkt';
    const PRODUCT_DELETION_WARNING = 'Nie można usunąć produktu na który złożono zamówienie.';
    const PRODUCT_DELETION_CONFIRMATION = 'Usunąć produkt?';

    $('.delete-category-btn').click(function (event) {
        let button = $(event.target);
        let categoryId = button.data('category-id');
        let modal = $("#deleteCategoryModal");
        let productCountPromise = getProductCountFromCategory(categoryId);
        productCountPromise.then(function (productCount) {
            let categoryName = button.data('category-name');

            modal.find('.modal-title').text(DELETE_CATEGORY + ' ' + categoryName);
            modal.find('.modal-body input').val(categoryId);
            let warningParagraph = modal.find('#productCountWarning');
            warningParagraph.text(PRODUCTS_AMOUNT_IN_CATEGORY + ': ' + productCount + '. ');
            if (productCount !== 0) {
                modal.find('button[type=submit]').prop('disabled', true);
                warningParagraph.append(CATEGORY_DELETION_WARNING);
            } else {
                modal.find('button[type=submit]').prop('disabled', false);
            }
            modal.modal();
        });
    });

    $("#newCategoryModal").on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let categoryId = button.data('category-id');
        let categoryName = button.data('category-name');
        let modal = $(this);
        modal.find('.modal-title').text(ADD_CATEGORY_TO + ' ' + categoryName);
        modal.find('.modal-body select').val(categoryId);
        setTimeout(function (){
            $('#modalName').focus();
        }, 500);
    });

    $('.delete-product-btn').click(function (event) {
        event.stopPropagation();
        let button = $(event.target);
        let productId = button.data('product-id');
        let modal = $("#deleteProductModal");
        let orderPositionCount = getOrderPositionCountWithProduct(productId);
        orderPositionCount.then(function (orderPositionCount) {
            let productName = button.data('product-name');

            modal.find('.modal-title').text(DELETE_PRODUCT + ' ' + productName);
            modal.find('.modal-body input').val(productId);
            let warningParagraph = modal.find('#orderPositionCountWarning');
            if (orderPositionCount !== 0) {
                warningParagraph.text(PRODUCT_DELETION_WARNING);
                modal.find('button[type=submit]').prop('disabled', true);
            } else {
                warningParagraph.text(PRODUCT_DELETION_CONFIRMATION);
                modal.find('button[type=submit]').prop('disabled', false);
            }
            modal.modal();
        });
    });
});
