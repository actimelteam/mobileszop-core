$( document ).ready(function() {
    $('.categoryHider').click(function () {
        $(this).parent().next('.childCategoryContainer').slideToggle('fast');
        console.log($(this).html());
        if ($(this).html() === '▼') {
            $(this).html('➤');
        } else {
            $(this).html('&#9660;');
        }
    })
});