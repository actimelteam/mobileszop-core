var pathArray = window.location.pathname.split('/');
var orderStatus = pathArray[pathArray.length - 1];
var listHeader = document.getElementById("list-header");
switch (orderStatus) {
    case "submitted":
        listHeader.innerText = "Przyjęte zamówienia";
        break;

    case "ready_to_collect":
        listHeader.innerText = "Zamówienia gotowe do odebrania";
        break;

    case "canceled":
        listHeader.innerText = "Anulowane zamówienia";
        break;

    case "realised":
        listHeader.innerText = "Zakończone zamówienia";
        break;
    case "orders":
        listHeader.innerText = "Wszystkie zamówienia";
        break;
}