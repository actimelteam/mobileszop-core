$(document).ready(function () {
    $(".autosubmit").change(function () {
        this.closest("form").submit();
    });
    $(".notPropagabale").click(function (event) {
        event.stopPropagation();
    });
});